"""
Adapted from - Luciano Ramalho - Fluent Python_ Clear, Concise, and Effective
Programming-O’Reilly Media (2015)
"""
import sys, locale


expressions = """
        default if output goes to text file: locale.getpreferredencoding(),
        check the type of the file: type(my_file),
        check the encoding of the file: my_file.encoding,
        True if output goes to console: sys.stdout.isatty(),
        encoding if output goest to console: sys.stdout.encoding,
        True if shell inputs enabled: sys.stdin.isatty(),
        encoding for shell inputs: sys.stdin.encoding,
        True if debug information: sys.stderr.isatty(),
        encoding for degugging: sys.stderr.encoding,
        used by Python to convert binary to/from str:sys.getdefaultencoding(),
        to endoce/decode filenames:sys.getfilesystemencoding()
        """

my_file = open('dummy', 'w')
for expression in expressions.split(','):
    comment, code = expression.split(':')
    value = eval(code.strip())
    print(code.strip().rjust(30), '-> ', repr(value))

print('-'*80)

for expression in expressions.split(','):
    comment, code = expression.split(':')
    print(code.strip().rjust(30), '-> ', comment.strip())
