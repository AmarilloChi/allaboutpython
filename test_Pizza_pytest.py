import pytest
from Pizza import Pizza

def test_Pizza():
    a_pizza = Pizza(16, 'Hawaiian')
    assert a_pizza.style == 'Hawaiian'

def test_Pizza_topping():
    a_pizza = Pizza(16, 'Hawaiian')
    a_pizza.add_topping('pineapple')
    assert a_pizza.topping == 'pineapple'

