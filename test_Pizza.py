from unittest import TestCase
from Pizza import Pizza


class TestPizza(TestCase):

    def setUp(self) -> None:
        self.a_pizza = Pizza(16, 'Hawaiian')

    def test_add_topping(self):
        self.a_pizza.add_topping('pineapple')
        self.assertEqual(self.a_pizza.topping, 'pineapple')

    def test_style(self):
        self.assertEqual(self.a_pizza.style, 'Hawaiian')

    def test_size(self):
        self.assertEqual(self.a_pizza.size, 16)
